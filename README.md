# hfive

A simple HDF5 file viewer for the terminal. Written in python using the h5py and
urwid libraries.

## Installation

```bash
pip install hfive
```

