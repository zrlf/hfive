# CHANGELOG for hfive

## 0.1.2 (2024-05-09)
- FIX: remove typehints to support python >3.7

## 0.1.1 (2024-05-08)
- FIX: fix issue that the bamboost cli would quit when the used hfive is exited
